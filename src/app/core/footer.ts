import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-footer',
    template: `
    <footer>
        <mat-card>
                <div>Jeżeli szkolenie <b>podobało Ci się</b>, będę miał prośbę o rekomendację. Dodaj się proszę do moich kontaktów na linkedin
                    <a mat-stroked-button
                        color="accent"
                        target="_blank"
                        href="https://www.linkedin.com/in/robertgurgul">
                        <img src="/assets/in.png"
                            width="15"
                            class="float-left mr-3">
                        mój profil
                    </a>
                </div>
        </mat-card>
    </footer>
  `,
})

export class FooterComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
