import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-init',
  template: `
    <h1>Szkolenie Programowanie reaktywne - RxJs</h1>
    <p>
      Programowanie reaktywne dotyczy asynchronicznych zdarzeń.
      <br>
      Strumienie (obiekty nasłuchujące) zdarzeń to np. aktywność użytownika, odpowiedź serwera, czy intervał.
      <br>
      Dane ze strumini możemy modyfikować lub łączyć.
    </p>

    <mat-card>
      więcej informacji
      <a target="_blank" mat-stroked-button color="accent" href="https://www.learnrxjs.io">strona do nauki</a>
      <a target="_blank" mat-stroked-button color="accent" href="http://reactivex.io">strona biblioteki</a>
    </mat-card>
  `,
  styles: []
})
export class InitComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
