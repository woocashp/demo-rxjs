import { Component } from '@angular/core';
const { version } = require('../../package.json')

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  version = version;
  nav = ['⌂', 'streams', 'operators', 'examples'];
}
