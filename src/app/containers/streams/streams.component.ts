import { subjects } from './streams/subjects';
import { observables } from './streams/observables';
import { Component } from '@angular/core';
import { DisplayStreamValue } from '../../utils/display-stream';

@Component({
  selector: 'app-streams',
  templateUrl: './streams.component.html'
})
export class StreamsComponent extends DisplayStreamValue {

  streamTypes = {
    observables: observables(this.display.bind(this)),
    subjects: subjects(this.display.bind(this), this.displayLine.bind(this))
  };

  constructor() {
    super();
  }

  run(fn: Function) {
    fn();
  }

}
