import { Api } from './../../../utils/api';
import { from, fromEvent, interval, of, range, timer } from "rxjs";
import { ajax } from "rxjs/ajax";
import { map, take } from "rxjs/operators";

export const observables = (display: Function) => {
  return {
    of: () => {
      of('of example').subscribe(display('of', 'Tworzy nowy (skończony) strumień.'));
    },

    interval: () => {
      interval(200).pipe(take(5)).subscribe(display('interval', 'Strumień zwraca wartość w danym interwale'));
    },

    timer: () => {
      timer(1000, 100).pipe(take(3)).subscribe(display('timer', 'Strumień zwraca wartość w danym interwale. Może rozpocząć z opóźnieniem.'));
    },

    range: () => {
      range(10, 10).subscribe(display('range', 'Strumień danych w podanym zakresie'));
    },

    fromEvent: () => {
      fromEvent(document.body, 'mousemove')
        .pipe(take(10), map((e: any) => ({ x: e.clientX })))
        .subscribe(display('fromEvent', 'Strumień zdarzeń użytkownika. Tutaj mousemove'));
    },

    from: () => {
      from(['Mike', 'Bob', 'Ele']).subscribe(display('from', 'Tworzenie strumienia z tablicy'));
    },

    ajax: () => {
      ajax(Api.DATA_ITEMS).pipe(map((resp) => resp.response)).subscribe(display('ajax', 'Komunikacja HTTP'));
    }
  }
}
