import { webSocket } from 'rxjs/webSocket';
import { take } from 'rxjs/operators';
import { Subject, ReplaySubject, BehaviorSubject, AsyncSubject } from 'rxjs';

export const subjects = (display: Function, displayLine: Function) => {
    return {
        Subject: () => {
            const subject$ = new Subject();
            subject$.pipe(take(2)).subscribe(display('subject', 'Obiekt Subject umożliwia emitowanie własnych danych.'));
            subject$.next(1);
            subject$.next(2);
        },

        ReplaySubject: () => {
            const rs$ = new ReplaySubject();
            rs$.next('a');
            rs$.next('b');
            rs$.pipe(take(3)).subscribe(display('ReplaySubject', 'Zapamiętuje wartości wyemitowane przed podłączeniem subskrybenta.'));
            rs$.next('c');
        },

        BehaviorSubject: () => {
            const bs$: BehaviorSubject<any> = new BehaviorSubject({
                id: 1,
                name: 'Kris'
            });
            bs$.subscribe(display('BehaviorSubject', 'Obiekt z początkową wartością. Wartośc może odczytać nie tylko subskybent.'));
            console.log(bs$.getValue());
            bs$.complete();
        },

        AsyncSubject:()=>{
            const as$ = new AsyncSubject();
            as$.subscribe(display('AsyncSubject', 'Emituje ostatnią wartość strumienia w momencie jego zakończenia.'));
            as$.next(1);
            as$.next(2);
            as$.next(3);
            as$.complete();
        },

        websocket: () => {
            const ws$ = webSocket({
                url: 'wss://echo.websocket.org',
                openObserver: {
                    next: (msg) => {
                        displayLine('NEXT', 'Server handshake. Status: ' + msg.type);
                    }
                }
            });
            ws$.next('welcome!');
            ws$.pipe(take(1)).subscribe(display('websocket message', 'Komunikacja z serwerem WebSocket'));
        }
    }
}
