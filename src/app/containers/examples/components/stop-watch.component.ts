import { Observable, of } from 'rxjs';
import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { fromEvent, interval } from 'rxjs';
import { mapTo, switchMap, scan, tap, map, delay, share } from 'rxjs/operators';

import { merge } from 'rxjs';
import { startWith } from 'rxjs/operators';

interface State {
  count: boolean;
  speed: number;
  value: number;
}

@Component({
  selector: 'stop-watch',
  styles: [`
  .wrapper {
    text-align: center;
    background: green;
    color: yellow;
    display: inline-block;
    border-radius: 50%;
    padding: 30px;
    margin: 20px;
    width: 100px;
    height: 100px;
  }
  .hidden {
    display: none;
  }
  #counter {
    font-size: 35px;
  }
  `],
  template: `
  <p>Użyte operatory: fromEvent,mapTo, startWith, scan, tap, switchMap, share.</p>
  <div class="wrapper">
    <div id="counter">{{((counter$|async)?.value || 0)/100|number : '1.2-2'}}</div>
    <br>
    <div id="controls">
      <button [class.hidden]="(counter$|async)?.count" #start>start</button>
      <button [class.hidden]="!(counter$|async)?.count" #pause>pause</button>
      <button #reset>reset</button>
    </div>
  </div>
  `
})

export class StopWatchComponent implements AfterViewInit {

  counter$!: Observable<State>;
  @ViewChild('start') start!: ElementRef;
  @ViewChild('pause') pause!: ElementRef;
  @ViewChild('reset') reset!: ElementRef;

  ngAfterViewInit(): void {

    const events$ = merge(
      fromEvent(this.start.nativeElement, 'click').pipe(mapTo({ count: true })),
      fromEvent(this.pause.nativeElement, 'click').pipe(mapTo({ count: false })),
      fromEvent(this.reset.nativeElement, 'click').pipe(mapTo({ value: 0 }))
    );

    const initValue = {
      count: false,
      speed: 10,
      value: 0
    };

    this.counter$ = events$.pipe(
      startWith(initValue),
      scan((state: State, curr: any): State => ({ ...state, ...curr }), initValue),
      switchMap((state: State) => state.count
        ? interval(state.speed).pipe(tap(_ => ++state.value), map(() => ({ ...state })))
        : of(state)
      ),
      share()
    );

  }

}
