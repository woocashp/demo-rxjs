import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { interval, merge, Observable } from "rxjs";
import {
  map,
  groupBy,
  mergeMap,
  scan,
  takeUntil,
  first,
  share,
  tap,
} from "rxjs/operators";

@Component({
  template: `
    <h3>
      Gra Rzut kostką.Gracze (Mike, Kris, Joe) losują liczbę od 1-6. Gra się
      kończy kiedy któryś z graczy zdobędzie łącznie min. 21 punktów. Dzięki
      operatorowi groupBy tworzymy strumień dla każdego użytkownika i możemy
      zliczać jego punkty w tym strumieniu. <br />
      Użyte operatory: groupBy, mergeMap, share, takeUntil, first, map
    </h3>

    artykuł: http://dataquarium.io/blog/fun-with-rxjs-groupby
    <div #display></div>
    <div *ngFor="let item of result$ | async">
      {{ item | json }}
    </div>
    <br />
    <b>the winner is: {{ (winner$ | async)?.name }}</b>
  `,
})
export class DiceComponent implements OnInit {
  winner$: Observable<any>;

  @ViewChild("display") display!: ElementRef;
  result$: any;

  constructor() {
    const players = ["Mike", "Kris", "Joe"];

    const game$ = interval(200).pipe(
      map((i) => ({
        name: players[i % players.length],
        value: Math.ceil(Math.random() * 6),
      })),
      groupBy((obj) => obj.name),
      mergeMap((player$) =>
        player$.pipe(
          scan(
            (acc, player) => ({
              name: player.name,
              score: acc.score + player.value,
            }),
            { score: 0 }
          )
        )
      ),
      share()
    );

    this.winner$ = game$.pipe(first((val) => val.score >= 21));

    this.result$ = merge(
      this.winner$,
      game$.pipe(takeUntil(this.winner$))
    ).pipe(
      scan((acc: any[], val) => [...acc, val], [])
    );
  }

  ngOnInit() { }
}
