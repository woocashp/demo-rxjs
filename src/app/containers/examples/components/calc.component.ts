import { Component, AfterViewInit, ViewChild } from "@angular/core";
import { combineLatest, fromEvent, iif, Observable, of } from "rxjs";
import { map, mergeMap } from "rxjs/operators";

@Component({
  selector: "app-calc",
  template: `
    <h3>Prosty kalkulator kalorii</h3>
    <h4>Przykład pokazuje zastosowanie operatora combineLatest, mergeMap, iif</h4>
    <input placeholder="your weight (number)" type="number" autofocus #weight>
    <b> * 24 * </b>
    <input placeholder="activity from 1.0 to 1.6 (number)" type="number" #activity>

    <ng-template #ok let-data>
        <div style="color: lightgreen">
            You should eat <b>{{data?.value}}</b> calories to maintain weight.
        </div>
    </ng-template>
    <ng-template #fail let-data>
        <div style="color: red;">{{data?.error}}</div>
    </ng-template>
    <hr>
    <ng-container *ngTemplateOutlet="(result$|async)?.value ? ok : fail; context: {$implicit: result$|async}"></ng-container>
    <br>
    `,
})

export class CalorieCalcComponent implements AfterViewInit {
  @ViewChild('weight') weight: any;
  @ViewChild('activity') activity: any;
  @ViewChild('output') output: any;
  result$!: Observable<{ value?: any, error?: any }>;

  ngAfterViewInit() {

    let events$ = combineLatest([
      fromEvent<Event>(this.weight.nativeElement, 'input'),
      fromEvent<Event>(this.activity.nativeElement, 'input')
    ]).pipe(
      map(([w, a]: Event[]) => {
        return {
          weight: +(w.target as HTMLInputElement).value,
          activity: +(a.target as HTMLInputElement).value
        };
      })
    )

    this.result$ = events$
      .pipe(
        mergeMap((obj) => iif(() => obj.weight > 0 && obj.activity >= 1 && obj.activity <= 1.6,
          of(obj).pipe(map(obj => ({ value: (obj.weight * 24 * obj.activity).toFixed() }))),
          of({ error: 'błędne dane wejściowe. Waga > 1; aktywność w zakresie 1 - 1.6' }))
        )
      );
  }
}
