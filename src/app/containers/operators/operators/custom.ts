import { customDouble, customMultiple } from './../custom-operators';
import { of } from "rxjs";

export const custom = (display: Function) => {
    return {
        double() {
            const els = of(1, 2, 3);
            els.pipe(
                customDouble
            ).subscribe(display('double', 'Przykład własnego pajpa'));
        },
        multiple() {
            const els = of(100);
            els.pipe(
                customMultiple(10)
            ).subscribe(display('multiple', 'Przykład własnego pajpa'));
        }
    }
}
