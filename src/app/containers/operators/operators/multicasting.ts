import { map, shareReplay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { share, tap } from 'rxjs/operators';
import { Api } from '../../../utils/api';

const loading = (obs: Observable<any>, displayLine: Function) => {
  displayLine('SIDE-EFFECT', 'start')
  // ten subskryber nie wywołuje ponownego zapytania do API.
  obs.subscribe(() => displayLine('SIDE-EFFECT', 'stop'));
}
/**
 * Multicasting to szeroki temat.
 * Więcej tutaj: https://itnext.io/the-magic-of-rxjs-sharing-operators-and-their-differences-3a03d699d255
 */
export const multicasting = (display: Function, displayLine: Function) => {
  return {
    share() {
      const req$ = ajax(Api.DATA_ITEMS).pipe(
        map((resp: any) => resp.response.data),
        tap(console.log), // będzie uruchomiony tylko raz
        share() // Zamienia Cold observable na Hot
      )
      req$.subscribe(display('share', 'Zamienia Cold observable na Hot. Jeżeli dane już istnieją, będą współdzielone pomiędzy subskrybentami do momentu odłączenia ostatniego subskrybenta.'));
      loading(req$, displayLine);
      /*
      Tutaj jednak pójdzie kolejny request ponieważ strumień się zakończył.
      setTimeout(() => {
        loading(req$, displayLine);
      }, 300); */
    },
    shareReplay() {
      const req$ = ajax(Api.DATA_ITEMS).pipe(
        map((resp: any) => resp.response.data),
        shareReplay(1)
      )
      req$.subscribe(display('shareReplay', 'Współdzielenie danych dla strumini. Podpięcie kolejnego subskrybenta (po zakończeniu strumienia) nie powoduje kolejnego requestu.'));
      loading(req$, displayLine);

      setTimeout(() => {
        loading(req$, displayLine);
      }, 300);
    }
  }
}
