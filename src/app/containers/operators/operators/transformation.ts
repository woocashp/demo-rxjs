import { map, scan, take } from 'rxjs/operators';
import { interval, range } from "rxjs";

export const transformation = (display: Function) => {
  return {
    map() {
      range(1000, 10)
        .pipe(map((value, idx) => value - idx * 2))
        .subscribe(display('map', 'modyfikuje wartość strumienia'));
    },
    scan() {
      interval(100).pipe(take(5))
        .pipe(scan((acc: number[], item) => {
          return [...acc, item];
        }, []))
        .subscribe(display('scan', 'Przetwarza i kumuluje dane strumienia'));
    }
  }
}
